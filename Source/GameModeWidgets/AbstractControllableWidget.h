// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "AbstractControllableWidget.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, Abstract)
class GAMEMODEWIDGETS_API UAbstractControllableWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UAbstractControllableWidget(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "GameMode")
	void HandleInput(class APlayerController* playerController, FVector2D axis);
	void HandleInput_Implementation(class APlayerController* playerController, FVector2D axis);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "GameMode")
	void Confirm(class APlayerController* playerController);
	void Confirm_Implementation(class APlayerController* playerController);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "GameMode")
	void Cancel(class APlayerController* playerController);
	void Cancel_Implementation(class APlayerController* playerController);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Gamepad")
	void UpdateVisual();
	virtual void UpdateVisual_Implementation();

	UFUNCTION(BlueprintCallable, Category = "Internal")
	void ResetNavigation();

	UFUNCTION(BlueprintCallable, Category = "Query")
	UWidget* GetSelectedElement();

	UFUNCTION(BlueprintCallable, Category = "Query")
	bool CanNavigate();
	
	UFUNCTION(BlueprintCallable, Category = "Initialization")
	void LoadWidgetsFromContainer(class UPanelWidget* widgetContainer);

	FVector2D CalculateIncrement(int32 Index, FVector2D axis);

public:
	UPROPERTY(BlueprintReadOnly, Category = "Configuration")
	bool NavigateInCycle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Configuration")
	int32 NavigationDelay;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Configuration")
	int32 HorizontalIncrement;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Configuration")
	int32 VerticalIncrement;

	UPROPERTY(BlueprintReadOnly, Category = "Configuration")
	bool UpdateVisualOnTick;

	UPROPERTY(BlueprintReadOnly, Category = "Internal")
	int32 MaxNavigationIndex;

	UPROPERTY(BlueprintReadOnly, Category = "Internal")
	int32 CurrentIndex;

	UPROPERTY(BlueprintReadOnly, Category = "Internal")
	FTimespan NavTimespan;

	UPROPERTY(BlueprintReadOnly, Category = "Internal")
	TArray<class UWidget*> NavigableWidgetList;

	UPROPERTY(BlueprintReadOnly, Category = "Internal")
	TMap<class APlayerController*, int32> ControllerToPlayerMap;
	
};
