// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UExampleWidget.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, Abstract)
class GAMEMODEWIDGETS_API UUExampleWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UUExampleWidget(const FObjectInitializer& ObjectInitializer);

	// Override "Event Construct" eventr
	virtual void NativeConstruct() override;

	// Override "Tick" event
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	// Custom function
	UFUNCTION(BlueprintCallable, Category = "Test Function")
	bool TestFunction();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Test Function")
	void ImplementableTestFunction(int32 value);
	virtual void ImplementableTestFunction_Implementation(int32 value);

};
