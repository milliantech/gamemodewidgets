// Fill out your copyright notice in the Description page of Project Settings.

#include "AbstractControllableWidget.h"

#include "Widget.h"
#include "PanelWidget.h"

UAbstractControllableWidget::UAbstractControllableWidget(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, NavigateInCycle(true)
	, NavigationDelay(0.3)
	, HorizontalIncrement(1)
	, VerticalIncrement(1)
	, UpdateVisualOnTick(true)
	, MaxNavigationIndex(0)
	, CurrentIndex(0)
	, NavTimespan(FDateTime::Now().GetTimeOfDay())
{

}

void UAbstractControllableWidget::Confirm_Implementation(class APlayerController* playerController)
{
	UE_LOG(LogTemp, Log, TEXT("Default Confirm Implementation"));
}

void UAbstractControllableWidget::Cancel_Implementation(class APlayerController* playerController)
{
	UE_LOG(LogTemp, Log, TEXT("Default Cancel Implementation"));
}

void UAbstractControllableWidget::HandleInput_Implementation(class APlayerController* playerController, FVector2D axis)
{

}

void UAbstractControllableWidget::UpdateVisual_Implementation()
{

}

void UAbstractControllableWidget::ResetNavigation()
{
	NavTimespan = FDateTime::Now().GetTimeOfDay();
}

UWidget* UAbstractControllableWidget::GetSelectedElement()
{
	return NavigableWidgetList[CurrentIndex];
}

bool UAbstractControllableWidget::CanNavigate()
{
	FTimespan currentTimespan = FDateTime::Now().GetTimeOfDay();
	FTimespan intervalSpan = NavTimespan + FTimespan::FromMilliseconds(FMath::TruncToFloat(NavigationDelay * 1000.0));
	if (currentTimespan > intervalSpan)
	{
		ResetNavigation();
		return true;
	}

	return false;
}

void UAbstractControllableWidget::LoadWidgetsFromContainer(class UPanelWidget* widgetContainer)
{
	UE_LOG(LogTemp, Log, TEXT("LoadWidgetsFromContainer Cpp"));
	int32 childrenCount = widgetContainer->GetChildrenCount();
	for (int32 i = 0; i < childrenCount; i++)
	{
		NavigableWidgetList.AddUnique(widgetContainer->GetChildAt(i));
	}

	MaxNavigationIndex = NavigableWidgetList.Num();
}

FVector2D UAbstractControllableWidget::CalculateIncrement(int32 Index, FVector2D axis)
{
	const int32 LocalCurrentIndex = Index;
	int32 IncrementValue = VerticalIncrement;
	/*
	if (direction == ENavDirection::Left || direction == ENavDirection::Right)
	{
		IncrementValue = HorizontalIncrement;
	}

	if (IncrementValue <= 0)
	{
		return LocalCurrentIndex;
	}

	if (direction == ENavDirection::Left || direction == ENavDirection::Up)
	{
		// Decrement Value
		if (LocalCurrentIndex > 0)
		{
			int32 Calculated = LocalCurrentIndex - IncrementValue;
			if (Calculated < 0)
			{
				return LocalCurrentIndex;
			}
			else
			{
				return Calculated;
			}
		}
		else
		{
			if (NavigateInCycle)
			{
				return MaxNavigationIndex - 1;
			}
			else
			{
				return LocalCurrentIndex;
			}
		}
	}
	else
	{
		// Increment Value
		if (LocalCurrentIndex < (MaxNavigationIndex - 1))
		{
			int32 Calculated = LocalCurrentIndex + IncrementValue;
			if (Calculated >(MaxNavigationIndex - 1))
			{
				return LocalCurrentIndex;
			}
			else
			{
				return Calculated;
			}
		}
		else
		{
			if (NavigateInCycle)
			{
				return 0;
			}
			else
			{
				return LocalCurrentIndex;
			}
		}
	}
	*/


	return FVector2D(0.0f, 0.0f);
}