// Fill out your copyright notice in the Description page of Project Settings.

#include "UExampleWidget.h"

#include <EngineGlobals.h>
#include <Runtime/Engine/Classes/Engine/Engine.h>

UUExampleWidget::UUExampleWidget(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

void UUExampleWidget::NativeConstruct()
{
	Super::NativeConstruct();

	UE_LOG(LogInit, Verbose, TEXT("ExampleWidget Initialized"));
}

void UUExampleWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
}

bool UUExampleWidget::TestFunction()
{
	UE_LOG(LogTemp, Warning, TEXT("ExampleWidget Test Function"));
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("This is an on screen message!"));

	return false;
}

void UUExampleWidget::ImplementableTestFunction_Implementation(int32 value)
{
	UE_LOG(LogTemp, Warning, TEXT("ExampleWidget Implementable Test Function with value %d"), value);
}